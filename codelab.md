id: play-with-git
summary: formation git
authors: @k33g_org

# Initiation à Git

<!-- ------------------------ -->
## 👋 Bonjour

Duration: 3

Le but de cette formation est d'apprendre à utiliser **Git**.

- Nous allons "fonctionner" à base d'exemples
- Nous ne verrons pas tout, juste l'essentiel > 📘
- Kit de survie 🧰

Positive
: - **Une règle importante**: n'hésitez pas à poser des questions (si je n'ai pas la réponse, je vous recontacterais plus tard) et **pour me joindre plus tard:** [pcharriere@gitlab.com](mailto:pcharriere@gitlab.com)

<!-- ------------------------ -->
## Tour de table

Duration: 5

- Philippe Charrière
- Technical Account Manager chez GitLab
- Différents métiers dans l'IT depuis 1995
- 🖐️ je mets des emojis partout

**Et vous ???**

<!-- ------------------------ -->
## Git

Duration: 5

- **Git** est un **DVCS** (Distributed Version Control System)
- Développé en 2005 par Linus Torvalds
- De plus en plus utilisé (le + utilisé?)
- Décentralisé 🤔 <> CVS, SVN

Positive
: note personnelle: Git est une machine à remonter le temps

Negative
: pas besoin de serveur pour faire du git

<!-- ------------------------ -->
## Pré-Requis

Duration: 2

- 📝 un éditeur de texte: VSCode (ou votre éditeur de texte préféré)
- 🖥 un terminal
- **git**


<!-- ------------------------ -->
## Configuration

Duration: 5

**1ère vérification**: dans un terminal (console) tapez `git --version`

### Configurer l'utilisateur de Git

```bash
git config --global user.email 'votre.email@domain.com'
git config --global user.name 'Bob Morane'
```

Positive:
: Bien sûr adaptez 😉

### Vérifiez

```bash
git config --get user.name
git config --get user.email
```

Negative
: Ces informations apparaîtront quand vous consulterez l'historique de vos projets

<!-- ------------------------ -->
## Le plus souvent

Duration: 3

Les commandes que vous utiliserez le plus sont les suivantes:

```bash
git init
git add .
git commit -m "📝 update"
git push # ce sera pour la session suivante
```

Positive
: les emojis ne sont pas obligatoires

Nous sommes prêts pour commencer 🎉

<!-- ------------------------ -->
## Git Init

Duration: 3

`git init` c'est la commande qui permet d'**initialiser** un répertoire comme un **"repository git"**

Maintenant, à vous de travailler.

### Création d'un dépôt (repository)

**👋 c'est à vous**

```bash
mkdir once-upon-a-time
cd once-upon-a-time
git init
```

Positive
: la commande crée une arborescence dans le répertoire dans un dossier caché `.git` qui va permettre de gérer tout l'historique

```bash
.git
├── config
├── description
├── HEAD
├── hooks
│  ├── applypatch-msg.sample
│  ├── commit-msg.sample
│  ├── fsmonitor-watchman.sample
│  ├── post-update.sample
│  ├── pre-applypatch.sample
│  ├── pre-commit.sample
│  ├── pre-push.sample
│  ├── pre-rebase.sample
│  ├── pre-receive.sample
│  ├── prepare-commit-msg.sample
│  └── update.sample
├── info
│  └── exclude
├── objects
│  ├── info
│  └── pack
└── refs
   ├── heads
   └── tags
```

<!-- ------------------------ -->
## Git status

Duration: 10

C'est la commande qui va permettre d'afficher l'état de vos fichiers dans le dépôt (repository)

Dans votre nouveau répertoire, **👋 tapez**:

```bash
git status
```

Vous devriez obtenir:

```bash
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```

Positive
: la 1ère ligne `On branch master` fait référence la **branche** `master`, vous pouvez voir ça comme la "version en cours/principale/par défaut/ ..." de votre projet. Mais pour le moment, ne vous en préoccupez pas, nous y reviendrons plus tard.

### Un fichier peut être à 3 "endroits" différents

... ou avoir 3 états différents pendant que vous travaillez

- Répertoire de travail *(c'est le système de fichiers, c'est le dossier du projet tel qu'il est stocké sur le disque)*
- Index de staging *(une sorte de mise en cache)*
- Dépôt *(c'est tout l'historique)*

**👋 tapez** (toujours dans le même répertoire):

```bash
touch README.md
echo "# Once Upon a Time" >> README.md
```

Negative
: cela revient à créer un fichier `README.md` avec une ligne de contenu `# Once Upon a Time`

Puis:

```bash
git status
```

Vous devriez obtenir quelque chose comme ceci:

```bash
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        README.md ✋
nothing added to commit but untracked files present (use "git add" to track)
```

Negative
: donc `README.md` n'est pas suivi par git, mais git l'a "détecté. Donc on peut dire que **`README.md` est dans le répertoire de travail**

Donc votre fichier est ici:

- Répertoire travail: 📌 **README.md**
- Index
- Dépôt

Positive
: **commande utile**: 💡 lister les fichiers non suivis: `git ls-files --others`

Negative
: ✋ faire une parenthèse sur le **Markdown**

<!-- ------------------------ -->
## Git add ou comment "passer" les fichiers dans la zone Index (de staging)

Duration: 5

Nous allons utiliser une nouvelle commande `git add`

**👋 tapez** (toujours dans le même répertoire):

```bash
git add README.md
```

Puis:

```bash
git status
```

Vous devriez obtenir:

```bash
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   README.md
```

Negative
: **Donc `README.md` est passé dans la zone Index**

Donc votre fichier est ici:

- Répertoire travail
- Index: 📌 **README.md**
- Dépôt

Positive
: **Tips**: pour passer **tous** les fichiers d'un coup dans la zone Index: `git add .`

## Donc quelques commandes pratiques

Duration: 1

- `git status`
- `git ls-files --others` pour lister les fichiers non gérés (dans le **Répertoire** de travail)
- `git ls-files` pour lister les fichiers dans **Index**
- `git ls-files --stage` la même chose mais avec l'identifiant du fichier sous forme de hash

Par exemple:

```bash
100644 3fa4f8eb97ca492998afc6ef016afcd2c08d3ac8 0       README.md
```

<!-- ------------------------ -->
## Où est `README.md`?

Duration: 2

```bash
.git
├── config
├── description
├── HEAD
├── hooks
│  ├── ...
│  └── ...
├── index
├── info
│  └── exclude
├── objects
│  ├── 3f 👋
│  │  └── a4f8eb97ca492998afc6ef016afcd2c08d3ac8 👋
│  ├── info
│  └── pack
└── refs
   ├── heads
   └── tags
```

**👋 donc `README.md` est "intégré" dans le système de Git, mais il n'est pas encore présent dans l'historique du dépôt**


<!-- ------------------------ -->
## Git commit ou comment "passer" les fichiers dans la zone Dépôt

Duration: 5

**LA** commande la plus importante, celle qui "valide" votre travail

```bash
git commit -m "🎉 mon 1er commit"
```

**👋 vous pouvez la taper**, rappelez vous, votre fichier `README.md` est dans la zone **Index** (vous avez tapez `git add README.md`)

Le résultat de la commande devrez ressembler à ceci:

```bash
[master (root-commit) 1318ac0] 🎉 mon 1er commit
 1 file changed, 1 insertion(+)
 create mode 100644 README.md
```

**👋 si vous faites un**:

```bash
git status
```

Vous obtiendrez:

```bash
On branch master
nothing to commit, working tree clean
```

Votre fichier `README.md` et maintenant dans le **Dépôt**:

- Répertoire travail
- Index
- Dépôt: 📌 **README.md** 🎉

<!-- ------------------------ -->
## Mais que s'est-il passé dans mon projet?

Duration: 5

### Git log

La commande `git log` vous permet de voir ce qui a été "commité" sur votre projet:

```bash
commit 1318ac0d60dc87345168d994281fcc9f77056c34 (HEAD -> master)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 09:44:16 2020 +0100

    🎉 mon 1er commit
```

Positive
: La commande vous donne aussi l'identifiant unique du commit. Pour "sortir des logs" utilisez `q`, vous pouvez vous déplacer avec les flèches **haut** & **bas**

Negative
: Si vous n'arrivez pas à sortir des logs, essayez `:q`

**👋 faites un git log de vote projet**

### Blob

Une nouvelle commande: `git ls-tree`. **👋 tapez la commande ci-dessous**:

```bash
git ls-tree -r HEAD
```

Vous allez obtenir:

```bash
100644 blob 3fa4f8eb97ca492998afc6ef016afcd2c08d3ac8    README.md
```

Positive
: Le **blob** est un objet spécifique à **Git** qui stocke l'intégralité du fichier

Et vous en retrouverez la trace dans l'arborescence de `.git`

```bash
.git
├── COMMIT_EDITMSG
├── ...
├── logs
│  ├── HEAD
│  └── refs
│     └── heads
│        └── master 👋
├── objects
│  ├── 3f
│  │  └── a4f8eb97ca492998afc6ef016afcd2c08d3ac8
│  ├── 13
│  │  └── 18ac0d60dc87345168d994281fcc9f77056c34 👋
│  ├── 22
│  │  └── 34f2006250d5689e1c554bb8b881167c986cbd 👋 
│  ├── info
│  └── pack
└── refs
   ├── heads
   │  └── master 👋
   └── tags
```

<!-- ------------------------ -->
## Manipulation de fichiers

Duration: 3

Quelques nouvelles commandes:

- Renommer un fichier: `git mv note.txt notes.txt`
- Supprimer un fichier: `git rm todo.md`
- Arrêter de suivre un fichier: `git rm --cached todo.md`

### Ignorer des fichiers

Vous créez un fichier `.gitignore` dans votre dossier

```bash
touch .gitignore
echo "*.log" >> .gitignore
git add .gitignore
git commit -m "added .gitignore"
```

Et dans ce fichier, vous précisez les fichiers qui ne doivent pas être pris en compte par git. Donc dans notre cas les fichiers `*.log`

<!-- ------------------------ -->
## Règles de messages de commit

Duration: 3

- être explicite
- être court
- poser des règles au démarrage
- fun 🤔 [https://gitmoji.carloscuesta.me/](https://gitmoji.carloscuesta.me/)
  - 🐛 fixing bug `:bug:`
  - 🎨 improving code structure `:art:`
  - 🐳 struggle with docker `:whale:` *etc...*

<!-- ------------------------ -->
## Historique, logs, ...

Duration: 10

Une fonctionnalité importante d'un DVCS est de pouvoir retrouver "facilement" des informations.

**👋 travaillez un peu**:

- Ajouter une ligne dans le fichier `README.md`: `c'est l'histoire d'une 🐸`
- sauter une ligne et sauvegarder + `git add .; git commit`
- Ajouter une ligne dans le fichier `README.md`: `qui aimait aller à la 🏖️`
- sauter une ligne et sauvegarder + `git add .; git commit`

Positive
: vous pouvez remplacer les emojis par du texte

Negative
: n'oubliez pas le message de commit

### Git log

**👋 tappez la commande**:

```bash
git log
```

Vous devriez obtenir quelque chose comme ceci:

```
commit 693e56dd2bdf4ac7b39ac2d4bc949086ed39e960 (HEAD -> master)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 11:47:28 2020 +0100

    📝 add a 🏖️

commit 9c2858cf881f31588fe6700cd2f8b3825a04b0c4
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 11:46:50 2020 +0100

    📝 add a 🐸

commit 7c8f4239f7575c45276da3457b631ee414ff8f6d
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 11:23:14 2020 +0100

    added .gitignore
```

**👋 essayez ces autres commandes**:

```bash
git log -1
git log -2
git log -- README.md
```

Donc:

- Dernier commit: `git log -1`
- Les 2 derniers commits: `git log -2`
- Uniquement les commits d'un fichier: `git log -- README.md`

### Un peu plus de détail avec git log

**👋 essayez**:

```bash
git log -1 --stat
```

Vous devriez obtenir ceci (quelque chose d'approchant):

```bash
commit 693e56dd2bdf4ac7b39ac2d4bc949086ed39e960 (HEAD -> master)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 11:47:28 2020 +0100

    📝 add a 🏖️

 README.md | 3 +++
 1 file changed, 3 insertions(+)
```

### Affichage "condensé" avec git log

**👋 essayez**:

```bash
git log --oneline
```

Ce qui donnera:

```bash
693e56d (HEAD -> master) 📝 add a 🏖️
9c2858c 📝 add a 🐸
7c8f423 added .gitignore
507a22d changes
18fe9a4 save
bdb59b8 updates
dca4362 📝 add a note
1318ac0 🎉 mon 1er commit
```


<!-- ------------------------ -->
## Chercher les différences

Duration: 10

**👋 Ajoutez une ligne** au fichier `README.md`:

```markdown
Et qui était copine avec un 🐼
```

Sauvegardez et faites un:

```bash
git diff
```

Vous pouvez voir les changements effectués (cf. le `+`)

```diff
diff --git a/README.md b/README.md
index 8478215..2ed6337 100644
--- a/README.md
+++ b/README.md
@@ -4,3 +4,4 @@ c'est l'histoire d'une 🐸
 
 qui aimait aller à la 🏖️
 
+Et qui était copine avec un 🐼
\ No newline at end of file
```

**👋 Modifiez à nouveau**: enlevez les lignes blanches

Faites à nouveau un:

```bash
git diff
```

Vous notez les lignes préfixées par des `-` qui correspondent aux lignes supprimées:

```diff
diff --git a/README.md b/README.md
index 8478215..5a25bea 100644
--- a/README.md
+++ b/README.md
@@ -1,6 +1,5 @@
 # Once Upon a Time
 
 c'est l'histoire d'une 🐸
- 👈
 qui aimait aller à la 🏖️
- 👈
+Et qui était copine avec un 🐼
\ No newline at end of file
(END)
```


<!-- ------------------------ -->
## D'autres commandes utiles

Duration: 10

### Chercher un mot

Par exemple: rechercher le commit correspondant à l'ajout de `histoire` dans `README.md`

```bash
git log -S histoire
```

Positive
: vous pouvez aussi utiliser des expressions régulières

**👋 testez la commande**:

### Annuler un commit

**👋Par exemple**, ajoutez une nouvelle ligne dans votre fichier, sauvegardez, commitez. Puis faites un:

```bash
git log
```

Par exemple:

```bash
commit cec971c14b32241986dd7c2e100a2b2b63dbe160 (HEAD -> master)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 14:39:26 2020 +0100

    📝 ...

commit fd0000377c4c9acaaadbcf5c91b3392887cdc828
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 14:35:41 2020 +0100

    👋

```

Retrouvez l'identifiant du commit et supprimez le avec:

```bash
git revert fd0000377c4c9acaaadbcf5c91b3392887cdc828
```

Positive
: git va vous demander de décrire la raison du "revert". Ensuite pour sortir de l'interface de saisie de message, utilisez `wq`

<!-- ------------------------ -->
## Les Branches

Duration: 10

une branche, c'est le moyen de travailler en parallèle sur une future version (ou un fix) sans toucher la version en cours (généralement `master`)

**Depuis toute à l'heure nous travaillons sur `master`**

Positive
: 👋 une bonne pratique est de ne pas committer sur `master`

### Ajoutons un nouveau chapitre à notre histoire

... donc une nouvelle branche

**👋 tapez la commande**:

```bash
git checkout -b new-chapter
```

Qui devrait vous afficher ceci:

```bash
Switched to a new branch 'new-chapter'
```

Tapez la commande ci-dessous pour vérifier que vous êtes positionnez sur la bonne branche:

```bash
git branch
```

### Modifions donc notre histoire (le fichier `README.md`)

Donc **👋 ajoutez ceci**:

```markdown
### On va au resto

🐸 va manger des 🍔 avec son ami le 🐼
```

Puis, tapez les commandes suivantes

```bash
git add.
git commit -m "🍔 resto"
```

Et enfin:

```bash
git log -3
```

Vous aurez une sortie comme celle-ci:

``` bash
commit 33285ff8983e9fa25d0c87e2e1b1602e86026c5d (HEAD -> new-chapter) 🖐️
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 14:51:31 2020 +0100

    🍔 resto

commit cec971c14b32241986dd7c2e100a2b2b63dbe160 (master) 🖐️
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 14:39:26 2020 +0100

    📝 ...

commit fd0000377c4c9acaaadbcf5c91b3392887cdc828
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 14:35:41 2020 +0100

    👋
```

Positive
: vous pouvez noter que le précédent commit était bien sur **master** et le dernier sur **new-chapter**

### Un rapport plus "compact"

Pour avoir quelque chose de moins verbeux, **👋 utilisez**:

```bash
git log --graph --oneline
```

Cela sera plus lisible:

```bash
* 33285ff (HEAD -> new-chapter) 🍔 resto
* cec971c (master) 📝 ...
* fd00003 👋
* 98204d8 📝 add a 🐸
* 7c8f423 added .gitignore
* 507a22d changes
* 18fe9a4 save
* bdb59b8 updates
* dca4362 📝 add a note
* 1318ac0 🎉 mon 1er commit
```

<!-- ------------------------ -->
## Les Branches: liste

Duration: 5

Si vous souhaitez avoir la liste des branches:

**👋 tapez cette commande**:

```bash
git branch
```

Vous obtiendrez vos 2 branches:

```bash
  master
* new-chapter
```

Positive
: `*` montre quelle est la branche de travail courante

<!-- ------------------------ -->
## Les Branches: Revenir à `master`

Duration: 5

```bash
git checkout master
```

Si vous retourner dans `README.md` vous allez voir que votre nouveau chapitre a disparu 🙀

Negative
: par acquit de conscience, vous pouvez faire un `git checkout new-chapter` pour vérifier que vous n'avez pas perdu votre travail 😉

<!-- ------------------------ -->
## Les Branches: on "merge" !!!

Duration: 10

**👋 Vérifiez** bien que vous êtes sur **master** et tapez la commande suivante:

```bash
git merge new-chapter
```

Vous devriez obtenir quelque chose comme cela:

```bash
Updating cec971c..33285ff
Fast-forward
 README.md | 4 ++++
 1 file changed, 4 insertions(+)
```

- Allez voir votre fichier `README.md`
- Votre contenu est fusionné 🎉

<!-- ------------------------ -->
## Les Branches: encore des chapitres

Duration: 5

### Chapitre 3

**👋 créez une nouvelle branche** `chapter-3`

```bash
git checkout -b chapter-3  
```

**👋 Ajoutez** un chapitre dans `README.md`

```markdown
### Chapter 3

🐸 aime les 🌺
```

N'oubliez pas de sauvegarder et committer:

```bash
git add .
git commit -m "add some 🌺"
```

### Chapitre 2 🤔

**👋 créez une nouvelle branche** `chapter-2`

```bash
git checkout master
git checkout -b chapter-2
```

**👋 Ajoutez** un chapitre dans `README.md`

```markdown
### Chapter 2

🐸 aime 🎃
```

Encore une fois, n'oubliez pas de sauvegarder et committer:

```bash
git add .
git commit -m "add a 🎃"
```

### Et si on fusionnait? 🤞(chapter-3) 🖐️ 3 pas 2 !!!

**👋 tapez ces commandes**:

```bash
git checkout master
git merge chapter-3  
```

```bash
Updating 33285ff..d18789d
Fast-forward
 README.md | 5 +++++
 1 file changed, 5 insertions(+)
```

### Et si on fusionnait? 🤞(chapter-2)

**👋 tapez cette commande**:

```bash
git merge chapter-2
```

Et là ... 💥

<!-- ------------------------ -->
## Conflit(s)

Duration: 10

Git a détecté un conflit. Vous devez avoir un message de ce type

```bash
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
```

**👋 ouvrez votre fichier `README.md`**, vous allez découvrir ceci:

```markdown
<<<<<<< HEAD
### Chapter 3

🐸 aime les 🌺
=======
### Chapter 2

🐸 aime 🎃
>>>>>>> chapter-2
```

Positive
: Git a ajouté des marques pour vous délimiter des conflits, à vous de modifier, sauvegarder et committer les changements corrigés.

### Résoudre le conflit

- Modifiez votre fichier
- Sauvegardez
- "Committez" les corrections


<!-- ------------------------ -->
## Un peu d'histoire

Duration: 5

### La version longue

**👋 tapez la commande suivante**:

```bash
git log -5 --graph
```

Vous obtiendrez ce rapport:

```bash
*   commit f98e4122e8d3d8cecd32b0c80a75a3a147333a7e (HEAD -> master)
|\  Merge: d18789d 35e8154
| | Author: Philippe Charrière <pcharriere@gitlab.com>
| | Date:   Tue Mar 17 15:14:41 2020 +0100
| | 
| |     📝 2 chapters added
| | 
| * commit 35e815480e450785946733888f6c2bd7a1f115be (chapter-2)
| | Author: Philippe Charrière <pcharriere@gitlab.com>
| | Date:   Tue Mar 17 15:08:29 2020 +0100
| | 
| |     add a 🎃
| | 
* | commit d18789d18bec4e072d8509c648740f649ff117e5 (chapter-3)
|/  Author: Philippe Charrière <pcharriere@gitlab.com>
|   Date:   Tue Mar 17 15:04:47 2020 +0100
|   
|       add some 🌺
| 
* commit 33285ff8983e9fa25d0c87e2e1b1602e86026c5d (new-chapter)
| Author: Philippe Charrière <pcharriere@gitlab.com>
| Date:   Tue Mar 17 14:51:31 2020 +0100
| 
|     🍔 resto
```

### La version courte

**👋 tapez la commande suivante**:

```bash
git log -5 --graph --oneline
```

Vous obtiendrez ce rapport:

```bash
*   f98e412 (HEAD -> master) 📝 2 chapters added
|\  
| * 35e8154 (chapter-2) add a 🎃
* | d18789d (chapter-3) add some 🌺
|/  
* 33285ff (new-chapter) 🍔 resto
* cec971c 📝 ...
```

<!-- ------------------------ -->
## Les Tags

Duration: 15

Les tags peuvent servir:

- à versionner un projet
- faciliter la maintenance
- ...

### Appliquer des tags

**👋 faites un**:

```bash
git log
```

Repérez le ou les commits qui vous intéressent:

```bash
commit 35e815480e450785946733888f6c2bd7a1f115be (chapter-2)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 15:08:29 2020 +0100

    add a 🎃

commit d18789d18bec4e072d8509c648740f649ff117e5 (chapter-3)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 15:04:47 2020 +0100

    add some 🌺
```

Ensuite appliquez un tag sur chacun de ces commits:

- un par branche
- vous pouvez les nommer comme vous voulez

```bash
git checkout 35e815480e450785946733888f6c2bd7a1f115be 
git tag chapter_2 👈
git checkout d18789d18bec4e072d8509c648740f649ff117e5 
git tag chapter_3 👈
git checkout master
```

Positive
: une bonne pratique est d'utiliser un numéro de version

Negative
: pour obtenir la liste des tags, utilisez cette commande `git tag`

### Afficher les tags dans l'historique

Tapez cette commande:

```bash
git log -5 --graph --oneline
```

Vous noterez qu'apparaissent les endroits où vous avez appliqué des tags:

```bash
*   f98e412 (HEAD -> master) 📝 2 chapters added
|\  
| * 35e8154 (tag: chapter_2, chapter-2) add a 🎃
* | d18789d (tag: chapter_3, chapter-3) add some 🌺
|/  
* 33285ff (new-chapter) 🍔 resto
* cec971c 📝 ...
```

<!-- ------------------------ -->
## Imaginez qu'il faille faire un ajout au chapitre 2

Duration: 15

### Lire le Contenu du chapitre 2

**👋Utilisez** le **tag** pour cela:

```bash
git show chapter_2
```

Vous devriez obtenir quelque chose comme ceci:

```bash
commit 35e815480e450785946733888f6c2bd7a1f115be (tag: chapter_2, chapter-2)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Tue Mar 17 15:08:29 2020 +0100

    add a 🎃

diff --git a/README.md b/README.md
index 7bc9f11..2fa62d4 100644
--- a/README.md
+++ b/README.md
@@ -9,3 +9,8 @@ Et qui était copine avec un 🐼
 ### On va au resto
 
 🐸 va manger des 🍔 avec son ami le 🐼
+
+
+### Chapter 2
+
+🐸 aime 🎃
\ No newline at end of file
```

### Se "positionner" sur le chapitre 2 (le tag donc)

**👋 tapez la commande**:

```bash
git checkout chapter_2
```

Une fois sur sur le **tag**, créez une nouvelle branche `chapter_2_addon` à partir de celui-ci:

```bash
git checkout -b chapter_2_addon
```

### Mettre à jour le chapitre 2 (le contenu)

**👋 changez** le texte du chapitre 2 dans `README.md`

```markdown
### Chapter 2

🐸 aime 🎃
🐸 aime 🎅
```

Et enfin, faites 1 Commit + 1 merge:

```bash
git add .
git commit -m "addon"  
git tag chapter_2_latest
git checkout master
git merge chapter_2_addon
```

Fixez les conflits (si il y en a), puis:

```bash
git add .
git commit -m "chapter 2 updated"
```

Et enfin, pour "regarder" l'historique:

```bash
git log -5 --graph --oneline  
```  

Cet historique devrait être similaire à ceci:

```bash
*   849380c (HEAD -> master) chapter 2 updated
|\  
| * c0b5ec2 (tag: chapter_2_latest, chapter_2_addon) addon
* |   f98e412 📝 2 chapters added
|\ \  
| |/  
| * 35e8154 (tag: chapter_2, chapter-2) add a 🎃
* | d18789d (tag: chapter_3, chapter-3) add some 🌺
|/  
```

Positive
: ce mode de fonctionnement est intéressant pour faire des fixes et les reporter et aussi des fixes dans le passé.

<!-- ------------------------ -->
## 🍒 pick

Duration: 15

Imaginons que je décide de changer le titre à la fin du livre, et de reporter ça dans les versions précédentes.

**👋 Modifiez** le "grand titre" dans `README.md` sur `master`. Par exemple:

```markdown
# Once Upon a Time
```

devient:

```markdown
# Once Upon a Time 🏰
```

Sauvegardez, committez

```bash
git add .
git commit -m "title has changed"
```

Negative
: 👋 **attention ce n'est pas une bonne habitude de travailler directement sur master**

### 🍒 pick: reporter la modification "dans le passé"

**👋 passez cette commande** pour récupérer l'identifiant du commit:

```bash
git log -1 --oneline
```

Dans mon cas, c'était `3fd9248`

Puis retournez sur la **branche** correspondant au chapitre 3:

```bash
git checkout chapter-3 # la branche pas le tag
```

Vérifiez le titre dans le fichier `README.md`

Et "appliquez" le commit avec la commande `cherry-pick`:

```bash
git cherry-pick 3fd9248
```

Allez vérifier dans votre fichier `README.md`, il a bien changé

Negative
: Vous pouvez refaire un `git checkout master` puis à nouveau un `git checkout chapter-3` pour vérifier

<!-- ------------------------ -->
## "Emergency"

Duration: 10

- 🚧 Répertoire (ou Copie) de travail
- 📝 Index
- 📦 Dépôt local

### git checkout: défaire la modification en cours

#### hello 1

```bash
touch README.md
echo "1. Hello" > README.md # c'est sauvegardé, dans Copie de travail 🚧
git add . # on passe les data dans Index 📝

cat README.md

   📝  │ 1. Hello
```

#### hello 2

```bash
echo "2. Hello" >> README.md # dans Copie de travail 🚧

cat README.md

   📝  │ 1. Hello
   🚧+ │ 2. Hello
```

Je voudrais annuler ma dernière action:

```bash
git checkout README.md # on récupère la version qui est dans Index 📝

cat README.md

   📝  │ 1. Hello

```

Positive
: `git restore README.md` fonctionne aussi

Finalement:

```bash
echo "2. Hello" >> README.md # dans Copie de travail 🚧
git add . # dans Index 📝
git commit -m "update" # dans Dépôt 📦

cat README.md

   📦  │ 1. Hello
   📦  │ 2. Hello
```

### git checkout HEAD: défaire dans l'Index & Copie de travail

#### hello 3 et 4

```bash
echo "3. Hello" >> README.md # dans Copie de travail 🚧
git add . # dans Index 📝
echo "4. Hello" >> README.md # dans Copie de travail 🚧

cat README.md

   📦  │ 1. Hello
   📦  │ 2. Hello
   📝  │ 3. Hello
   🚧+ │ 4. Hello

```

Faites un:

```bash
git checkout HEAD README.md

cat README.md

   📦  │ 1. Hello
   📦  │ 2. Hello

```

Positive
: rappel `git checkout HEAD README.md` annule à la fois ce qu'il y a dans l'Index 📝 et le Répertoire de travail 🚧

Finalement, refaites:

```bash
echo "3. Hello" >> README.md # dans Copie de travail 🚧
git add . # dans Index 📝
echo "4. Hello" >> README.md # dans Copie de travail 🚧

cat README.md

   📦  │ 1. Hello
   📦  │ 2. Hello
   📝  │ 3. Hello
   🚧+ │ 4. Hello

```

### git reset: défaire seulement dans l'index

```bash
git reset README.md

cat README.md

   📦  │ 1. Hello
   📦  │ 2. Hello
   🚧+ │ 3. Hello
   🚧+ │ 4. Hello
```

Positive
: donc `git reset README.md` place le contenu de l'Index 📝 dans le Répertoire de travail 🚧

```bash
git add .
git commit -m "update bis"

git log

commit 53dd6e079f2b717ee574360f138c757a7eb966ee (HEAD -> master)
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Sun May 3 18:11:19 2020 +0200

    update bis

commit f44c3ac169f726b184792e2b5f658d0f88f9cdb9
Author: Philippe Charrière <pcharriere@gitlab.com>
Date:   Sun May 3 17:28:30 2020 +0200
```

```bash
# annulation du dernier commit
# on enlève du dépôt 📦 pour placer dans l'Index 📝, vérifier avec un git log
git reset --soft HEAD~1
# on remet l'Index 📝 dans le Répertoire de travail 🚧
git reset README.md

cat README.md

   📦  │ 1. Hello
   📦  │ 2. Hello
   🚧+ │ 3. Hello
   🚧+ │ 4. Hello
```

<!-- ------------------------ -->
## The End

Duration: 1

Voilà, Git permet encore plus de choses, mais vous avez le kit de survie nécessaire

<!-- ------------------------ -->
## 🖐️ One more thing: git push, git pull...

Démo

<!-- ------------------------ -->
## Ressources

- [https://git-scm.com/doc](https://git-scm.com/doc)
- [https://learngitbranching.js.org/](https://learngitbranching.js.org/?locale=fr_FR)
